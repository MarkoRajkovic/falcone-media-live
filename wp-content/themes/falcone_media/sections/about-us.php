<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 about_us_image menu_black d-none d-sm-block"
                 style="background-image: url('<?= $image_url; ?>')">
                <div class="col-xl-8 col-lg-8 col-md-8 col-12 text_about">
                    <h1 class="section_subtitle"><?= $title ?></h1>
                    <h2 class="section_title"><?= $subtitle ?></h2>
                    <span class="font_poppins">
                        <?= $content ?>
                    </span>
                </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-12 about_us_image d-block d-sm-none"
                 style="background-image: url('<?= $image_url_mobile; ?>')">
                <div class="col-xl-8 col-lg-8 col-md-8 col-12 text_about">
                    <h1 class="section_subtitle"><?= $title ?></h1>
                    <h2 class="section_title"><?= $subtitle ?></h2>
                    <span class="font_poppins">
                        <?= $content ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>

