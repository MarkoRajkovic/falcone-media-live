<?php
function loadStylesheetsAndScripts()
{
    wp_register_script('custom-script', get_template_directory_uri() . "/js/custom.js?" . time(), array('jquery'), null, false); //Load custom script
    wp_enqueue_script('custom-script'); // Enqueue it

    wp_register_style('google-fonts', "https://use.fontawesome.com/releases/v5.8.1/css/all.css"); //Load google fonts
    wp_enqueue_style('google-fonts'); // Enqueue it!

    wp_register_style('custom-css', get_template_directory_uri() . "/custom.css?" . time()); //Load slider css
    wp_enqueue_style('custom-css'); // Enqueue it!

    wp_register_style('style', get_stylesheet_uri() . '?' . time()); //Load default style.css
    wp_enqueue_style('style'); // Enqueue it!

    wp_register_script('ajax-bootstrap', get_template_directory_uri() . "/js/ajax.bootstrap.min.js"); //Load default style.css
    wp_enqueue_script('ajax-bootstrap'); // Enqueue it!

    wp_register_script('bootstrap', get_template_directory_uri() . "/js/bootstrap.min.js", array("jquery"), "4.0.0"); //Load bootstrap script
    wp_enqueue_script('bootstrap'); // Enqueue it!

}

add_action('wp_enqueue_scripts', 'loadStylesheetsAndScripts');
$postatts = array();
function createPosttypes()
{
    $postTypes = array(
        array(
            "name" => "Landing",
            "singular" => "Landing",
            "slug" => "landing",
        ),
        array(
            "name" => "Our Projects",
            "singular" => "Our Projects",
            "slug" => "ourprojects",
        ),
        array(
            "name" => "Services",
            "singular" => "Services",
            "slug" => "services",
        ),
    );
    foreach ($postTypes as $posttype) {
        $postOptions = array(
            'labels' => array(
                'name' => __($posttype["name"]),
                'singular_name' => __($posttype["singular"]),
            ),
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'page-attributes',
                'excerpt',
                'custom-fields'
            ),
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'page',
            'show_in_nav_menus' => true,
        );
        register_post_type($posttype["name"], $postOptions);
    }
}

add_action('init', 'createPosttypes');
//Register menus for theme
function register_my_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('Header menu'),
            'footer-menu' => __('Footer menu'),
        )
    );
}

add_action('init', 'register_my_menus');


//Disable admin bar for all in frontend
show_admin_bar(false);
//add picture to custom post type
add_theme_support('post-thumbnails');
function custom_search_form($form, $value = "Search", $post_type = 'post')
{
    $categories = get_terms(
        'category',
        array(
            'hide_empty' => 0,
            'number' => 3,
            'order' => 'asc',
            'parent' => 0,
            'post_types' => array('post'),
        )
    );
    $category = '';
    foreach ($categories as $cat) {
        $category .= "<li><span value='$cat->slug'>$cat->name</span></li>";
    }
    $form = '<form method="get" id="searchform" action="' . get_option('home') . '/" >
               <ul id="category" class="blog_categories">
                    <input type="hidden" name="post_type" value="' . $post_type . '"/>
                    <li class="active"><span value="">All Categories</span></li>
                    ' . $category . '
                </ul>
            </form>';
    return $form;
}

// the ajax function
add_action('wp_ajax_data_fetch', 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch', 'data_fetch');
function data_fetch()
{
    $searchTerms = array(
        'posts_per_page' => -1,
        's' => esc_attr($_POST['keyword']),
        'post_type' => 'post',
    );
    if (isset($_POST['category']) && $_POST['category'] != '') {
        $searchTerms['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => esc_attr($_POST['category']),
            )
        );
    }
    $the_query = new WP_Query($searchTerms);
    if ($the_query->have_posts()) :
        while ($the_query->have_posts()): $the_query->the_post();
            ?>
            <div class="col-xl-4 col-lg-4 col-md-4 col-12 blog_post">
                <a href="<?= get_permalink() ?>">
                    <?php the_post_thumbnail('image_blog'); ?>
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12 blog_info">
                        <span class="date_blog">
                            <?php echo get_the_date('M d, Y'); ?>
                        </span>
                        <h5>
                            <?php the_title(); ?>
                        </h5>
                        <span class="author_blog">
                            <?php echo get_avatar(get_the_author_meta('ID'), 30); ?>
                            by <?php the_author(); ?>
                        </span>
                    </div>
                </a>
            </div>
        <?php endwhile;
        wp_reset_postdata();
    endif;
    die();
}

//About us thumbs
require_once "components/AboutComponent.php";

/* two custom images for pages */
/*
add_action('after_setup_theme', 'custom_postimage_setup');
function custom_postimage_setup()
{
    add_action('add_meta_boxes', 'custom_postimage_meta_box');
    add_action('save_post', 'custom_postimage_meta_box_save');
}

function custom_postimage_meta_box()
{
    //on which post types should the box appear?
    $post_types = array('page', 'services');//post type
    foreach ($post_types as $pt) {
        add_meta_box('custom_postimage_meta_box', __('Other images', 'yourdomain'), 'custom_postimage_meta_box_func', $pt, 'side', 'low');
    }
}

function custom_postimage_meta_box_func($post)
{
    //an array with all the images (ba meta key). The same array has to be in custom_postimage_meta_box_save($post_id) as well.
    $meta_keys = array('image_1', 'image_2');
    $i = 0;
    foreach ($meta_keys as $meta_key) {
        if ($meta_keys == 0) {
        }
        $image_meta_val = get_post_meta($post->ID, $meta_key, true);
        ?>
        <div class="custom_postimage_wrapper" id="<?php echo $meta_key; ?>_wrapper" style="margin-bottom:20px;">
            <img src="<?php echo($image_meta_val != '' ? wp_get_attachment_image_src($image_meta_val)[0] : ''); ?>"
                 style="width:100%;display: <?php echo($image_meta_val != '' ? 'block' : 'none'); ?>" alt="">
            <a class="addimage button"
               onclick="custom_postimage_add_image('<?php echo $meta_key; ?>');"><?php $i == 0 ? _e('Image 1', 'yourdomain') : _e('Image 2', 'yourdomain'); ?></a><br>
            <a class="removeimage"
               style="color:#a00;cursor:pointer;display: <?php echo($image_meta_val != '' ? 'block' : 'none'); ?>"
               onclick="custom_postimage_remove_image('<?php echo $meta_key; ?>');"><?php _e('Remove image', 'yourdomain'); ?></a>
            <input type="hidden" name="<?php echo $meta_key; ?>" id="<?php echo $meta_key; ?>"
                   value="<?php echo $image_meta_val; ?>"/>
        </div>
        <?php $i++;
    } ?>
    <script>
        function custom_postimage_add_image(key) {
            var $wrapper = jQuery('#' + key + '_wrapper');
            custom_postimage_uploader = wp.media.frames.file_frame = wp.media({
                title: '<?php _e('select image', 'yourdomain'); ?>',
                button: {
                    text: '<?php _e('select image', 'yourdomain'); ?>'
                },
                multiple: false
            });
            custom_postimage_uploader.on('select', function () {
                var attachment = custom_postimage_uploader.state().get('selection').first().toJSON();
                var img_url = attachment['url'];
                var img_id = attachment['id'];
                $wrapper.find('input#' + key).val(img_id);
                $wrapper.find('img').attr('src', img_url);
                $wrapper.find('img').show();
                $wrapper.find('a.removeimage').show();
            });
            custom_postimage_uploader.on('open', function () {
                var selection = custom_postimage_uploader.state().get('selection');
                var selected = $wrapper.find('input#' + key).val();
                if (selected) {
                    selection.add(wp.media.attachment(selected));
                }
            });
            custom_postimage_uploader.open();
            return false;
        }

        function custom_postimage_remove_image(key) {
            var $wrapper = jQuery('#' + key + '_wrapper');
            $wrapper.find('input#' + key).val('');
            $wrapper.find('img').hide();
            $wrapper.find('a.removeimage').hide();
            return false;
        }
    </script>
    <?php
    wp_nonce_field('custom_postimage_meta_box', 'custom_postimage_meta_box_nonce');
}

function custom_postimage_meta_box_save($post_id)
{
    if (!current_user_can('edit_posts', $post_id)) {
        return 'not permitted';
    }
    if (isset($_POST['custom_postimage_meta_box_nonce']) && wp_verify_nonce($_POST['custom_postimage_meta_box_nonce'], 'custom_postimage_meta_box')) {
        //same array as in custom_postimage_meta_box_func($post)
        $meta_keys = array('image_1', 'image_2');
        foreach ($meta_keys as $meta_key) {
            if (isset($_POST[$meta_key]) && intval($_POST[$meta_key]) != '') {
                update_post_meta($post_id, $meta_key, intval($_POST[$meta_key]));
            } else {
                update_post_meta($post_id, $meta_key, '');
            }
        }
    }
}
*/
/* end two custom images for pages */
/*
// sample form with nonce
function pippin_sample_nonce_form() {

    ob_start(); ?>

    <form id="pippin_nonce_sample" method="POST" action="" style="margin-top: 150px">
        <p>Name
            <input type="text" name="name" value="" required style="margin-bottom: 15px;"/><br>
            Last Name
            <input type="text" name="name" value="" required style="margin-bottom: 15px;"/><br>
            Email
            <input type="text" name="name" value="" required style="margin-bottom: 15px;"/><br>
            <input type="hidden" name="pippin_sample_nonce" value="<?php echo wp_create_nonce('pippin-sample-nonce'); ?>"/>
            <input type="submit" value="Submit"/>
        </p>
    </form>
    <?php
    return ob_get_clean();
}
add_shortcode('nonce_form', 'pippin_sample_nonce_form');

// processes the data submitted by the form
function pippin_process_form_data() {

    if(isset($_POST['pippin_sample_nonce'])) {
        if(wp_verify_nonce($_POST['pippin_sample_nonce'], 'pippin-sample-nonce')) {

            echo 'Nonce verified successfully'; exit;
            // process form here

        } else {
            echo 'nonce verification failed'; exit;
        }
    }
}
add_action('init', 'pippin_process_form_data');
function nonce_hourly() {
    return 10;
}
add_filter( 'nonce_life', 'nonce_hourly' );

$complete_url = wp_nonce_url( $bare_url, 'delete-user_'.$user->ID, ‘my_nonces’ );
*/
