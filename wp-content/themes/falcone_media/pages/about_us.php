<?php

/* Template Name: About us template */

get_header();

?>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12 about_us_image d-none d-sm-block"
                     style='background-image: url("<?php echo the_post_thumbnail_url(); ?>")'>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12 about_us_image about_blur d-none d-sm-block js-blur"
                         style='background-image: url("<?= wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'image_1', true), 'full')[0]?>")'>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12 text_about">
                        <?php while (have_posts()) : the_post(); ?>

                            <?php the_content(); ?>

                        <?php endwhile; ?>
                        </span>
                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12 col-12 about_us_image d-block d-sm-none"
                     style='background-image: url("<?= wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'image_2', true), 'full')[0]?>")'>

                    <div class="col-xl-8 col-lg-8 col-md-8 col-12 text_about">
                        <?php while (have_posts()) : the_post(); ?>

                            <?php the_content(); ?>

                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>